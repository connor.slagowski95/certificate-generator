# Certificate Generator

Access student information from the Canvas API using OAuth2 to generate course completion certificates.