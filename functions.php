<?php

    $authorize_url = "http://cehe.instructure.com/login/oauth2/auth";
    $token_url = "https://cehe.instructure.com/login/oauth2/token";
    $course_api_url = "https://cehe.instructure.com/api/v1/courses/?per_page=100";
    if(isset($_POST['course'])) {
        $course = $_POST['course'];
        $student_api_url = "https://cehe.instructure.com/api/v1/courses/".$course."/students/";
    }
    if(isset($_POST['selectStudent'])) {
        $stu = $_POST['selectStudent'];
        $grade_api_url = "https://cehe.instructure.com/api/v1/users/".$stu."/enrollments";
    }
    
    

    //	callback URL specified when the application was defined--has to match what the application says
    $callback_uri = "https://www.iucdt.com/apps/certApp/chooseCourse.php";


    //	client (application) credentials - located at apim.byu.edu
    $client_id = (client_id_place_holder);
    $client_secret = (client_secret_place_holder);



    if (isset($_POST["authorization_code"])) {
        //	what to do if there's an authorization code
        $access_token = getAccessToken($_POST["authorization_code"]);
        $resource = getCourse($access_token);
    } elseif (isset($_GET["code"])) {
        $access_token = getAccessToken($_GET["code"]);
        $resource = getCourse($access_token);
    } else {
        //	what to do if there's no authorization code
        getAuthorizationCode();
    }



    //	step A - simulate a request from a browser on the authorize_url
    //		will return an authorization code after the user is prompted for credentials
    function getAuthorizationCode() {
        global $authorize_url, $client_id, $callback_uri;

        $authorization_redirect_url = $authorize_url . "?response_type=code&client_id=" . $client_id . "&redirect_uri=" . $callback_uri;

        header("Location: " . $authorization_redirect_url);

        //	if you don't want to redirect
        // echo "Go <a href='$authorization_redirect_url'>here</a>, copy the code, and paste it into the box below.<br /><form action=" . $_SERVER["PHP_SELF"] . " method = 'post'><input type='text' name='authorization_code' /><br /><input type='submit'></form>";
    }

   
    //	step I, J - turn the authorization code into an access token, etc.
    function getAccessToken($authorization_code) {
        global $token_url, $client_id, $client_secret, $callback_uri;

        //$authorization = base64_encode("$client_id:$client_secret");
        $header = array("Content-Type: application/x-www-form-urlencoded");
        $content = "grant_type=authorization_code&client_id=".$client_id."&client_secret=".$client_secret."&code=".$authorization_code."&redirect_uri=".$callback_uri;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $token_url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $content
        ));
        $response = curl_exec($curl);
        
        curl_close($curl);

        $r = json_decode($response, true);
        $r = $r['access_token'];
        return  $r;
    }

    //	get the course information to access the students
    function getCourse($access_token) {
        global $course_api_url;
        
        $header = array("Authorization: Bearer {$access_token}");


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $course_api_url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $r = json_decode($response,true);
        return  $r;
    }


    if(isset($_POST['course'])) {
        function getGrades($access_token) {
            global $multigrade_api_url;
            
            $header = array("Authorization: Bearer {$access_token}");


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $multigrade_api_url,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CONNECTTIMEOUT => 600
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            $r = json_decode($response,true);
            return  $r;
        }

        function getAssignment($access_token) {
            global $assignment_api_url;
            
            $header = array("Authorization: Bearer {$access_token}");


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $assignment_api_url,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CONNECTTIMEOUT => 600
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            $r = json_decode($response,true);
            return  $r;
        }
    }

    if(isset($_POST['assn'])) {
        function getSubmission($access_token) {
            global $submission_api_url;
            
            $header = array("Authorization: Bearer {$access_token}");


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $submission_api_url,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            $r = json_decode($response,true);
            return  $r;
        }
        
    }

    //

    

?>
